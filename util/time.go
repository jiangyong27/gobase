package util

import (
	"fmt"
	"strconv"
	"time"
)


func Timestamp2String(timestamp int64) string {
	if timestamp == 0 {
		return "-"
	}
	return time.Unix(timestamp, 0).Format("2006-01-02 15:04:05")
}

func ParseTime(format string, val string) time.Time {
	t, _ := time.ParseInLocation(format, val, time.Local)
	return t
}

func Yesterday(today int32) int32 {
	t, _ := time.ParseInLocation("20060102", fmt.Sprintf("%d", today), time.Local)
	yesterday := t.AddDate(0, 0, -1)
	yesterdayStr := yesterday.Format("20060102")
	yesterdayInt, _ := strconv.Atoi(yesterdayStr)
	return int32(yesterdayInt)
}

func Tomorrow(today int32) int32 {
	t, _ := time.ParseInLocation("20060102", fmt.Sprintf("%d", today), time.Local)
	tomorrow := t.AddDate(0, 0, 1)
	tomorrowStr := tomorrow.Format("20060102")
	tomorrowInt, _ := strconv.Atoi(tomorrowStr)
	return int32(tomorrowInt)
}

func Time2Weekday(t time.Time) string {
	switch t.Weekday() {
	case time.Monday:
		return "星期一"
	case time.Tuesday:
		return "星期二"
	case time.Wednesday:
		return "星期三"
	case time.Thursday:
		return "星期四"
	case time.Friday:
		return "星期五"
	case time.Saturday:
		return "星期六"
	case time.Sunday:
		return "星期天"
	}
	return ""
}


