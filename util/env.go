package util

import (
	"flag"
	"os"
	"reflect"
	"strconv"
	"strings"
)

func UpdateByEnv(prefix, tag string, value interface{}) {
	tp := reflect.TypeOf(value).Elem()
	val := reflect.ValueOf(value).Elem()

	for i := 0; i < tp.NumField(); i++ {
		sType := tp.Field(i)
		sValue := val.Field(i)
		envtag := sType.Tag.Get(tag)
		key := strings.ToUpper(prefix) + "_" + strings.ToUpper(envtag)
		if prefix == "" {
			key = strings.ToUpper(envtag)
		}
		if sType.Type.Kind() == reflect.Ptr && sType.Type.Elem().Kind() == reflect.Struct {
			UpdateByEnv(key, tag, sValue.Interface())
			continue
		}
		if sType.Type.Kind() != reflect.String && sType.Type.Kind() != reflect.Bool &&
			sType.Type.Kind() != reflect.Int && sType.Type.Kind() != reflect.Int8 && sType.Type.Kind() != reflect.Int16 &&
			sType.Type.Kind() != reflect.Int32 && sType.Type.Kind() != reflect.Int64 && sType.Type.Kind() != reflect.Uint &&
			sType.Type.Kind() != reflect.Uint8 && sType.Type.Kind() != reflect.Uint16 && sType.Type.Kind() != reflect.Uint32 &&
			sType.Type.Kind() != reflect.Uint64 && sType.Type.Kind() != reflect.Float32 && sType.Type.Kind() != reflect.Float64 {
			continue
		}

		envValue := os.Getenv(key)
		if envValue == "" {
			continue
		}
		switch sValue.Kind() {
		case reflect.Bool:
			v, _ := strconv.ParseBool(envValue)
			sValue.SetBool(v)
		case reflect.String:
			sValue.SetString(envValue)
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			v, _ := strconv.ParseInt(envValue, 10, 64)
			sValue.SetInt(v)
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			v, _ := strconv.ParseUint(envValue, 10, 64)
			sValue.SetUint(v)
		case reflect.Float32, reflect.Float64:
			v, _ := strconv.ParseFloat(envValue, 64)
			sValue.SetFloat(v)
		}
	}
}

func UpdateByFlag(prefix, tag string, value interface{}) {

	flagValues := make(map[string]string)
	flag.VisitAll(func(f *flag.Flag) {
		if f.Value.String() == f.DefValue {
			return
		}
		flagValues[f.Name] = f.Value.String()
	})

	tp := reflect.TypeOf(value).Elem()
	val := reflect.ValueOf(value).Elem()

	for i := 0; i < tp.NumField(); i++ {
		sType := tp.Field(i)
		sValue := val.Field(i)
		envtag := sType.Tag.Get(tag)
		envtag = strings.ReplaceAll(envtag, "_", "-")
		key := strings.ToLower(prefix) + "-" + strings.ToLower(envtag)
		if prefix == "" {
			key = strings.ToUpper(envtag)
		}
		if sType.Type.Kind() == reflect.Ptr && sType.Type.Elem().Kind() == reflect.Struct {
			UpdateByFlag(key, tag, sValue.Interface())
			continue
		}
		if sType.Type.Kind() != reflect.String && sType.Type.Kind() != reflect.Bool &&
			sType.Type.Kind() != reflect.Int && sType.Type.Kind() != reflect.Int8 && sType.Type.Kind() != reflect.Int16 &&
			sType.Type.Kind() != reflect.Int32 && sType.Type.Kind() != reflect.Int64 && sType.Type.Kind() != reflect.Uint &&
			sType.Type.Kind() != reflect.Uint8 && sType.Type.Kind() != reflect.Uint16 && sType.Type.Kind() != reflect.Uint32 &&
			sType.Type.Kind() != reflect.Uint64 && sType.Type.Kind() != reflect.Float32 && sType.Type.Kind() != reflect.Float64 {
			continue
		}

		if _, ok := flagValues[key]; !ok {
			continue
		}

		switch sValue.Kind() {
		case reflect.Bool:
			v, _ := strconv.ParseBool(flagValues[key])
			sValue.SetBool(v)
		case reflect.String:
			sValue.SetString(flagValues[key])
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			v, _ := strconv.ParseInt(flagValues[key], 10, 64)
			sValue.SetInt(v)
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			v, _ := strconv.ParseUint(flagValues[key], 10, 64)
			sValue.SetUint(v)
		case reflect.Float32, reflect.Float64:
			v, _ := strconv.ParseFloat(flagValues[key], 64)
			sValue.SetFloat(v)
		}
	}
}
