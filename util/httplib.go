package util

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

type HttpClient struct {
	req *http.Request
	rsp *http.Response

	param   map[string]string
	timeout time.Duration
}

func HttpGet(rawurl string) *HttpClient {
	return newHttpClient(rawurl, "GET")
}

func HttpPost(rawurl string) *HttpClient {
	return newHttpClient(rawurl, "POST")
}

func HttpPut(rawurl string) *HttpClient {
	return newHttpClient(rawurl, "PUT")
}

func HttpDelete(rawurl string) *HttpClient {
	return newHttpClient(rawurl, "DELETE")
}

func newHttpClient(rawurl string, method string) *HttpClient {
	u, err := url.Parse(rawurl)
	if err != nil {
		log.Println("url:", rawurl, "error:")
		return nil
	}

	req := &http.Request{
		URL:        u,
		Method:     method,
		Header:     make(http.Header),
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
	}

	rsp := &http.Response{}

	return &HttpClient{
		req:     req,
		rsp:     rsp,
		timeout: 10 * time.Second,
		param:   make(map[string]string),
	}
}

func (h *HttpClient) Cookie(cookie string) *HttpClient {
	h.req.Header.Add("Cookie", cookie)
	return h
}

func (h *HttpClient) Param(key, value string) *HttpClient {
	h.param[key] = value
	return h
}

func (h *HttpClient) Header(key, value string) *HttpClient {
	h.req.Header.Add(key, value)
	return h
}

func (h *HttpClient) BasicAuth(username, password string) *HttpClient {
	h.req.SetBasicAuth(username, password)
	return h
}

func (h *HttpClient) UserAgent(userAgent string) *HttpClient {
	h.req.Header.Set("User-Agent", userAgent)
	return h
}

func (h *HttpClient) Timeout(timeout time.Duration) *HttpClient {
	h.timeout = timeout
	return h
}

func (h *HttpClient) Body(byts []byte) *HttpClient {
	if byts != nil {
		h.req.Body = ioutil.NopCloser(bytes.NewReader(byts))
		h.req.ContentLength = int64(len(byts))
		h.req.Header.Set("Content-Type", "application/json")
	}

	return h
}

func (h *HttpClient) PostValues(urlValues url.Values) *HttpClient {

	byts := []byte(urlValues.Encode())
	h.req.Body = ioutil.NopCloser(bytes.NewReader(byts))
	h.req.ContentLength = int64(len(byts))
	h.req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return h
}

func (h *HttpClient) doResponse() (err error) {

	query := h.req.URL.Query()
	for k, v := range h.param {
		query.Add(k, v)
	}
	h.req.URL.RawQuery = query.Encode()

	client := http.Client{
		Timeout: h.timeout,
	}
	h.rsp, err = client.Do(h.req)
	if err != nil {
		return err
	}
	return nil
}

func (h *HttpClient) ToByte() ([]byte, error) {
	err := h.doResponse()
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(h.rsp.Body)
	if err != nil {
		return nil, err
	}

	h.rsp.Body.Close()
	return body, err
}

func (h *HttpClient) ToString() (string, error) {
	body, err := h.ToByte()
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func (h *HttpClient) ToFile(filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	err = h.doResponse()
	if err != nil {
		return err
	}

	if h.rsp.Body == nil {
		return nil
	}
	defer h.rsp.Body.Close()
	_, err = io.Copy(f, h.rsp.Body)
	return err
}

func (h *HttpClient) ToJSON(v interface{}) error {
	data, err := h.ToByte()
	if err != nil {
		return err
	}
	return json.Unmarshal(data, v)
}

func (h *HttpClient) ToXML(v interface{}) error {
	data, err := h.ToByte()
	if err != nil {
		return err
	}
	return xml.Unmarshal(data, v)
}

func (h *HttpClient) GetHeader(key string) string {
	return h.rsp.Header.Get(key)
}

func Download(url, path string) error {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	stat, err := f.Stat()
	if err != nil {
		return err
	}
	defer f.Close()

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Range", "bytes="+strconv.FormatInt(stat.Size(), 10)+"-")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
