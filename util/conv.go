package util

import (
	"fmt"
	"strconv"
)

// convert interface to string.
func GetString(v interface{}) string {
	switch result := v.(type) {
	case string:
		return result
	case []byte:
		return string(result)
	default:
		if v != nil {
			return fmt.Sprintf("%v", result)
		}
	}
	return ""
}

// convert interface to int.
func GetInt(v interface{}) int {
	switch result := v.(type) {
	case int:
		return result
	case int32:
		return int(result)
	case int64:
		return int(result)
	default:
		if d := GetString(v); d != "" {
			value, _ := strconv.Atoi(d)
			return value
		}
	}
	return 0
}

// convert interface to int64.
func GetInt64(v interface{}) int64 {
	switch result := v.(type) {
	case int:
		return int64(result)
	case int32:
		return int64(result)
	case int64:
		return result
	case float32:
		return int64(result)
	case float64:
		return int64(result)
	default:

		if d := GetString(v); d != "" {
			value, _ := strconv.ParseInt(d, 10, 64)
			return value
		}
	}
	return 0
}

// convert interface to uint64.
func GetUInt64(v interface{}) uint64 {
	switch result := v.(type) {
	case int:
		return uint64(result)
	case int32:
		return uint64(result)
	case int64:
		return uint64(result)
	case uint64:
		return result
	default:

		if d := GetString(v); d != "" {
			value, _ := strconv.ParseUint(d, 10, 64)
			return value
		}
	}
	return 0
}

func GetInt64Slices(v interface{}) []int64 {

	switch result := v.(type) {

	case []int64:
		return []int64(result)
	default:
		fmt.Println("GetInt64Slices: ", result)
		return nil
	}
}

// convert interface to byte slice.
func GetByteArray(v interface{}) []byte {
	switch result := v.(type) {
	case []byte:
		return result
	case string:
		return []byte(result)
	default:
		return nil
	}
}

func GetUInt(v interface{}) uint {
	s, _ := strconv.ParseUint(GetString(v), 10, 64)
	return uint(s)
}

func GetUInt32(v interface{}) uint32 {
	s, _ := strconv.ParseUint(GetString(v), 10, 32)
	return uint32(s)
}

func GetUInt16(v interface{}) uint16 {
	s, _ := strconv.ParseUint(GetString(v), 10, 16)
	return uint16(s)
}

func GetUInt8(v interface{}) uint8 {
	s, _ := strconv.ParseUint(GetString(v), 10, 8)
	return uint8(s)
}

func GetInt32(v interface{}) int32 {
	s, _ := strconv.ParseInt(GetString(v), 10, 32)
	return int32(s)
}

func GetInt16(v interface{}) int16 {
	s, _ := strconv.ParseInt(GetString(v), 10, 16)
	return int16(s)
}

func GetInt8(v interface{}) int8 {
	s, _ := strconv.ParseInt(GetString(v), 10, 8)
	return int8(s)
}

func GetFloat32(v interface{}) float32 {
	f, _ := strconv.ParseFloat(GetString(v), 32)
	return float32(f)
}

func GetFloat64(v interface{}) float64 {
	f, _ := strconv.ParseFloat(GetString(v), 64)
	return f
}

func GetBool(v interface{}) bool {
	b, _ := strconv.ParseBool(GetString(v))
	return b
}
