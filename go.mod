module gitlab.com/jiangyong27/gobase

go 1.18

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/astaxie/beego v1.7.1
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/gin-gonic/gin v1.1.4
	github.com/gogap/errors v0.0.0-20210818113853-edfbba0ddea9
	github.com/gogap/stack v0.0.0-20150131034635-fef68dddd4f8 // indirect
	github.com/silenceper/wechat v1.2.6
	gorm.io/driver/mysql v1.3.3
	gorm.io/gorm v1.23.5
)
