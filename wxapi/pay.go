package wxapi

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/silenceper/wechat"
	"github.com/silenceper/wechat/cache"
	"github.com/silenceper/wechat/pay"
)

var (
	urlPayTransferWeixin = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"
	urlPayTransferBank   = "https://api.mch.weixin.qq.com/mmpaysptrans/pay_bank"
	urlPayGetPublicKey   = "https://fraud.mch.weixin.qq.com/risk/getpublickey"
	urlPayBillDownload   = "https://api.mch.weixin.qq.com/pay/downloadfundflow"
)

const (
	TransferTypeWeixin        = 1
	TransferTypeBank          = 2
	DownloadFundBillOperation = "Operation"
	DownloadFundBillBasic     = "Basic"
	DownloadFundBillFees      = "Fees"
)

type PayConfig struct {
	AppID        string
	AppSecret    string
	PayMchID     string //支付 - 商户 ID
	PayNotifyURL string //支付 - 接受微信支付结果通知的接口地址
	PayKey       string //支付 - 商户后台设置的支付 key
	PayCertFile  []byte
	PayKeyFile   []byte
	PayP12File   []byte
}

type PayPrepareRequest struct {
	TotalFee   int32
	RemoteIp   string
	Body       string
	OutTradeNo string
	OpenID     string
	TradeType  string
	NotifyUrl  string
}

type PayPrepareResponse struct {
	PrePayId  string `json:"pre_pay_id"`
	AppID     string `json:"app_id"`
	Timestamp int64  `json:"timestamp"`
	NonceStr  string `json:"noncestr"`
	Package   string `json:"package"`
	Sign      string `json:"sign"`
	SignType  string `json:"sign_type"`
}

type PayNativeRequest struct {
	Appid       string `xml:"appid"`
	IsSubscribe string `xml:"is_subscribe"`
	MchId       string `xml:"mch_id"`
	NonceStr    string `xml:"nonce_str"`
	Openid      string `xml:"openid"`
	ProductId   string `xml:"product_id"`
}

type PayTransfer struct {
	TransferType int
	BankNo       string
	BankCode     string
	Openid       string
	RealName     string
	Amount       int32
	TradeNo      string
	RemoteIp     string
	Desc         string
}

type PayCallback struct {
	Appid         string `xml:"appid"`
	BankType      string `xml:"bank_type"`
	CashFee       string `xml:"cash_fee"`
	FeeType       string `xml:"fee_type"`
	IsSubscribe   string `xml:"is_subscribe"`
	MchId         string `xml:"mch_id"`
	NonceStr      string `xml:"nonce_str"`
	Openid        string `xml:"openid"`
	OutTradeNo    string `xml:"out_trade_no"`
	Sign          string `xml:"sign"`
	TimeEnd       string `xml:"time_end"`
	TotalFee      string `xml:"total_fee"`
	TradeType     string `xml:"trade_type"`
	TransactionId string `xml:"transaction_id"`
}

type PayCallbackResponse struct {
	XMLName    xml.Name `xml:"xml"`
	ReturnCode string   `xml:"return_code"`
	ReturnMsg  string   `xml:"return_msg"`
}

type WxPay struct {
	wepay     *pay.Pay
	publicKey []byte
	tlsClient *http.Client
	stdClient *http.Client
}

func NewPay(cfg *PayConfig) *WxPay {
	c := new(wechat.Config)

	c.PayKey = cfg.PayKey
	c.PayMchID = cfg.PayMchID
	c.PayNotifyURL = cfg.PayNotifyURL
	c.AppSecret = cfg.AppSecret
	c.AppID = cfg.AppID
	c.Cache = cache.NewMemcache()
	wc := wechat.NewWechat(c)

	pay := &WxPay{
		wepay:     wc.GetPay(),
		stdClient: &http.Client{},
	}

	if cfg.PayCertFile != nil && cfg.PayKeyFile != nil {
		client, err := pay.withCert(cfg.PayCertFile, cfg.PayKeyFile)
		if err != nil {
			panic(err)
		}
		pay.tlsClient = client
	}

	if pay.tlsClient != nil {
		publicKey, err := pay.GetPublicKey()
		if err != nil {
			panic(err)
		}
		pay.publicKey = publicKey
	}

	return pay
}

// 发送请求
func (c *WxPay) post(url string, params params, tls bool) ([]byte, error) {
	var httpc *http.Client
	if tls {
		if c.tlsClient == nil {
			return nil, errors.New("tls wepay is not initialized")
		}
		httpc = c.tlsClient
	} else {
		httpc = c.stdClient
	}
	buf := bytes.NewBuffer(params.Encode())
	resp, err := httpc.Post(url, bodyTypeXml, buf)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

// 附着商户证书
func (c *WxPay) withCert(cert, key []byte) (*http.Client, error) {
	tlsCert, err := tls.X509KeyPair(cert, key)
	if err != nil {
		return nil, err
	}

	conf := &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
	}
	trans := &http.Transport{
		TLSClientConfig: conf,
	}

	return &http.Client{
		Transport: trans,
	}, nil

}

func (c *WxPay) GetPublicKey() ([]byte, error) {

	params := newParams()
	params.Set("mch_id", c.wepay.PayMchID)
	params.Set("nonce_str", randomStr(32))
	params.Set("sign_type", "MD5")
	params.Set("sign", params.SignMd5(c.wepay.PayKey))

	body, err := c.post(urlPayGetPublicKey, params, true)
	if err != nil {
		return nil, err
	}
	result := newParams()
	result.Decode(body)

	if result.GetString("return_code") != "SUCCESS" {
		return nil, errors.New(result.GetString("return_msg"))
	}

	if result.GetString("result_code") != "SUCCESS" {
		return nil, fmt.Errorf("%s:%s",
			result.GetString("err_code"), result.GetString("err_code_des"))
	}

	return []byte(result.GetString("pub_key")), nil
}

func (c *WxPay) PrepareJSAPI(prePayId string) (*PayPrepareResponse, error) {
	packageStr := "prepay_id=" + prePayId
	timestamp := time.Now().Unix()
	signType := "MD5"
	noceStr := randomStr(32)
	payParams := newParams()
	payParams.Set("appId", c.wepay.AppID)
	payParams.Set("nonceStr", noceStr)
	payParams.Set("timeStamp", timestamp)
	payParams.Set("package", packageStr)
	payParams.Set("signType", signType)

	rsp := new(PayPrepareResponse)
	rsp.PrePayId = prePayId
	rsp.Package = packageStr
	rsp.AppID = c.wepay.AppID
	rsp.Timestamp = timestamp
	rsp.NonceStr = noceStr
	rsp.SignType = signType
	rsp.Sign = payParams.SignMd5(c.wepay.PayKey)

	return rsp, nil
}

//统一下预支付订单
func (c *WxPay) PrepareOrder(prepareOrder *PayPrepareRequest) (*PayPrepareResponse, error) {
	params := new(pay.Params)
	params.OpenID = prepareOrder.OpenID
	params.TotalFee = fmt.Sprintf("%d", prepareOrder.TotalFee)
	params.Body = prepareOrder.Body
	params.CreateIP = prepareOrder.RemoteIp
	params.OutTradeNo = prepareOrder.OutTradeNo
	params.NotifyURL = prepareOrder.NotifyUrl
	if prepareOrder.TradeType == "" {
		params.TradeType = "JSAPI"
	} else {
		params.TradeType = prepareOrder.TradeType
	}

	payOrder, err := c.wepay.PrePayOrder(params)
	if err != nil {
		return nil, err
	}
	if strings.ToUpper(payOrder.ResultCode) != "SUCCESS" {
		return nil, fmt.Errorf("%s", payOrder.ResultCode)
	}

	packageStr := "prepay_id=" + payOrder.PrePayID
	timestamp := time.Now().Unix()
	signType := "MD5"

	payParams := newParams()
	payParams.Set("appId", payOrder.AppID)
	payParams.Set("nonceStr", payOrder.NonceStr)
	payParams.Set("timeStamp", timestamp)
	payParams.Set("package", packageStr)
	payParams.Set("signType", signType)

	rsp := new(PayPrepareResponse)
	rsp.PrePayId = payOrder.PrePayID
	rsp.Package = packageStr
	rsp.AppID = payOrder.AppID
	rsp.Timestamp = timestamp
	rsp.NonceStr = payOrder.NonceStr
	rsp.SignType = signType
	rsp.Sign = payParams.SignMd5(c.wepay.PayKey)

	return rsp, nil
}

// 计算NativeQrcocde支付的二维码内容
func (w *WxPay) NativeQrcode(productId string) string {
	params := newParams()
	nonceStr := randomStr(32)
	timestmp := time.Now().Unix()

	params.Set("appid", w.wepay.AppID)
	params.Set("mch_id", w.wepay.PayMchID)
	params.Set("nonce_str", nonceStr)
	params.Set("product_id", productId)
	params.Set("time_stamp", timestmp)
	sign := params.SignMd5(w.wepay.PayKey)

	return fmt.Sprintf("weixin://wxpay/bizpayurl?appid=%s&mch_id=%s&nonce_str=%s&product_id=%s&time_stamp=%d&sign=%s",
		w.wepay.AppID, w.wepay.PayMchID, nonceStr, productId, timestmp, sign)
}

//用户扫码之后的回调内容解析
func (c *WxPay) NativeQrcodeRequest(body []byte) (*PayNativeRequest, error) {
	params := newParams()
	params.Decode(body)

	if params.CheckSign(c.wepay.PayKey) == false {
		return nil, errors.New("sign error")
	}

	var req PayNativeRequest

	req.Appid = params.GetString("openid")
	req.MchId = params.GetString("mch_id")
	req.IsSubscribe = params.GetString("is_subscribe")
	req.ProductId = params.GetString("product_id")
	req.Openid = params.GetString("openid")

	return &req, nil
}

//用户扫码之后的回调内容应答
func (c *WxPay) NativeQrcodeResonse(prepareId string) []byte {
	params := newParams()
	params.Set("return_code", "SUCCESS")
	params.Set("return_msg", "SUCCESS")
	params.Set("appid", c.wepay.AppID)
	params.Set("mch_id", c.wepay.PayMchID)
	params.Set("nonce_str", randomStr(32))
	params.Set("prepay_id", prepareId)
	params.Set("result_code", "SUCCESS")
	params.Set("sign", params.SignMd5(c.wepay.PayKey))

	return params.Encode()
}

//企业付款
func (c *WxPay) Transfer(transfer *PayTransfer) error {

	params := newParams()
	params.Set("nonce_str", randomStr(32))
	params.Set("partner_trade_no", transfer.TradeNo)
	params.Set("amount", transfer.Amount)
	params.Set("desc", transfer.Desc)

	//付款到微信
	if transfer.TransferType == TransferTypeWeixin {
		params.Set("mchid", c.wepay.PayMchID)
		params.Set("mch_appid", c.wepay.AppID)
		params.Set("openid", transfer.Openid)
		if transfer.RealName != "" {
			params.Set("check_name", "FORCE_CHECK")
			params.Set("re_user_name", transfer.RealName)
		} else {
			params.Set("check_name", "NO_CHECK")
		}
		params.Set("spbill_create_ip", transfer.RemoteIp)

		//付款到银行卡
	} else if transfer.TransferType == TransferTypeBank {
		fmt.Println(string(c.publicKey), "name:", transfer.RealName, "bankno:", transfer.BankNo)
		bankNo, err := rsaEncrypt([]byte(transfer.BankNo), c.publicKey)
		if err != nil {
			return err
		}
		realName, err := rsaEncrypt([]byte(transfer.RealName), c.publicKey)
		if err != nil {
			return err
		}

		params.Set("mch_id", c.wepay.PayMchID)
		params.Set("enc_bank_no", base64.StdEncoding.EncodeToString(bankNo))
		params.Set("enc_true_name", base64.StdEncoding.EncodeToString(realName))
		params.Set("bank_code", transfer.BankCode)

	} else {
		return errors.New("transfer_type error")
	}

	url := urlPayTransferWeixin
	if transfer.TransferType == TransferTypeBank {
		url = urlPayTransferBank
	}
	params.Set("sign", params.SignMd5(c.wepay.PayKey))
	body, err := c.post(url, params, true)
	if err != nil {
		return err
	}
	result := newParams()
	result.Decode(body)

	if result.GetString("return_code") != "SUCCESS" {
		return errors.New(result.GetString("return_msg"))
	}

	if result.GetString("result_code") != "SUCCESS" {
		return fmt.Errorf("%s:%s",
			result.GetString("err_code"), result.GetString("err_code_des"))
	}
	return nil
}

func (c *WxPay) PayCallback(body []byte) (*PayCallback, error) {
	params := newParams()
	params.Decode(body)

	if params.GetString("return_code") != "SUCCESS" {
		return nil, fmt.Errorf("return_code:%s return_msg:%s",
			params.GetString("return_code"), params.GetString("return_msg"))
	}

	if !params.CheckSign(c.wepay.PayKey) {
		return nil, errors.New("sign failed")
	}

	var payCallback PayCallback

	payCallback.Appid = params.GetString("appid")
	payCallback.BankType = params.GetString("bank_type")
	payCallback.CashFee = params.GetString("cash_fee")
	payCallback.FeeType = params.GetString("fee_type")
	payCallback.IsSubscribe = params.GetString("is_subscribe")
	payCallback.MchId = params.GetString("mch_id")
	payCallback.Openid = params.GetString("openid")
	payCallback.OutTradeNo = params.GetString("out_trade_no")
	payCallback.TimeEnd = params.GetString("time_end")
	payCallback.TotalFee = params.GetString("total_fee")
	payCallback.TradeType = params.GetString("trade_type")
	payCallback.TransactionId = params.GetString("transaction_id")

	return &payCallback, nil
}

// 下载账单
func (c *WxPay) DownloadBill(date string, accountType string) ([]byte, error) {
	params := newParams()
	params.Set("appid", c.wepay.AppID)
	params.Set("mch_id", c.wepay.PayMchID)
	params.Set("nonce_str", randomStr(32))
	params.Set("bill_date", date)
	params.Set("account_type", accountType)
	params.Set("sign", params.SignHmacSha256(c.wepay.PayKey))

	url := urlPayBillDownload

	body, err := c.post(url, params, true)
	if err != nil {
		return nil, err
	}

	result := newParams()

	if err = result.Decode(body); err != nil {
		return body, nil
	}

	returnCode := result.GetString("return_code")
	if returnCode != "" && returnCode != "SUCCESS" {
		return nil, errors.New(result.GetString("return_msg"))
	}

	resultCode := result.GetString("result_code")
	if resultCode != "" && resultCode != "SUCCESS" {
		return nil, fmt.Errorf("%s:%s",
			result.GetString("err_code"), result.GetString("err_code_des"))
	}

	return body, nil
}
