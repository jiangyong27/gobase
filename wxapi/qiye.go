package wxapi

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/jiangyong27/gobase/util"
)

var (
	urlQiyeToken  = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
	urlQiyeSend   = "https://qyapi.weixin.qq.com/cgi-bin/message/send"
	urlQiyeUpload = "https://qyapi.weixin.qq.com/cgi-bin/media/upload"
)

type qiyeToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
}

type qiyeResponse struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

type qiyeBaseRequest struct {
	ToUser  string `json:"touser"`
	ToParty string `json:"toparty"`
	ToTag   string `json:"totag"`
	MstType string `json:"msgtype"`
	AgentId string `json:"agentid"`
}

type qiyeMessageRequest struct {
	qiyeBaseRequest
	Text struct {
		Content string `json:"content"`
	} `json:"text"`

	Image struct {
		MediaID string `json:"media_id"`
	} `json:"image"`

	Video struct {
		MediaID string `json:"media_id"`
		Title   string `json:"title"`
		Desc    string `json:"description"`
	} `json:"video"`

	TaskCard struct {
		Url         string         `json:"url"`
		Title       string         `json:"title"`
		Description string         `json:"description"`
		TaskId      string         `json:"task_id"`
		Btn         []*qiyeCardBtn `json:"btn"`
	} `json:"taskcard"`
}

type qiyeCardBtn struct {
	Key         string `json:"key"`
	Name        string `json:"name"`
	ReplaceName string `json:"replace_name"`
	Color       string `json:"color"`
	IsBold      bool   `json:"is_bolad"`
}

type qiyeUploadResponse struct {
	qiyeResponse
	MediaId  string `json:"media_id"`
	CreateAt string `json:"created_at"`
	Type     string `json:"type"`
}

// ===================================================================
type QiyeConfig struct {
	Corpid string
	Secret string
	Sender string
}

type WxQiye struct {
	corpid      string
	secret      string
	sender      string
	tokenUpdate int64
	token       string
}

func NewQiye(cfg *QiyeConfig) *WxQiye {
	return &WxQiye{
		corpid:      cfg.Corpid,
		secret:      cfg.Secret,
		sender:      cfg.Sender,
		tokenUpdate: 0,
	}
}

func (c *WxQiye) getToken() (string, error) {

	if time.Now().Unix()-c.tokenUpdate <= 3600 {
		return c.token, nil
	}

	url := fmt.Sprintf("%s?corpid=%s&corpsecret=%s",
		urlQiyeToken, c.corpid, c.secret)

	res, err := util.HttpGet(url).ToString()
	if err != nil {
		return "", err
	}

	var obj qiyeToken
	err = json.Unmarshal([]byte(res), &obj)
	if err != nil {
		return "", err
	}

	if obj.ExpiresIn != 0 {
		c.token = obj.AccessToken
		return c.token, nil
	} else {
		return "", errors.New(string(res))
	}
}

func (c *WxQiye) SendText(receiver []string, content string) error {

	token, err := c.getToken()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s?access_token=%s", urlQiyeSend, token)
	req := new(qiyeMessageRequest)
	req.MstType = "text"
	req.AgentId = c.sender
	req.Text.Content = content
	req.ToUser = strings.Join(receiver, "|")

	data, _ := json.Marshal(req)
	result, err := util.HttpPost(url).Body(data).ToString()
	if err != nil {
		return err
	}

	var rsp qiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *WxQiye) SendImage(receiver []string, meidiaId string) error {
	token, err := c.getToken()
	if err != nil {
		return err
	}

	url := fmt.Sprintf("%s?access_token=%s", urlQiyeSend, token)
	req := new(qiyeMessageRequest)
	req.MstType = "image"
	req.AgentId = c.sender
	req.ToUser = strings.Join(receiver, "|")
	req.Image.MediaID = meidiaId

	data, _ := json.Marshal(req)
	result, err := util.HttpPost(url).Body(data).ToString()
	if err != nil {
		return err
	}

	var rsp qiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}
	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}

	return nil
}

func (c *WxQiye) SendVideo(receiver []string, meidiaId, title, desc string) error {
	token, err := c.getToken()
	if err != nil {
		return err
	}

	url := fmt.Sprintf("%s?access_token=%s", urlQiyeSend, token)
	req := new(qiyeMessageRequest)
	req.MstType = "video"
	req.AgentId = c.sender
	req.ToUser = strings.Join(receiver, "|")
	req.Video.MediaID = meidiaId
	req.Video.Title = title
	req.Video.Desc = desc
	data, _ := json.Marshal(req)
	result, err := util.HttpPost(url).Body(data).ToString()
	if err != nil {
		return err
	}

	var rsp qiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}
	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}

	return nil
}

func (c *WxQiye) Upload(path, kind string) (string, error) {
	token, err := c.getToken()
	if err != nil {
		return "", err
	}

	fh, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer fh.Close()
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileWriter, err := bodyWriter.CreateFormFile(kind, filepath.Base(path))
	if err != nil {
		return "", err
	}

	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return "", err
	}
	bodyWriter.Close()

	url := fmt.Sprintf("%s?access_token=%s&type=%s", urlQiyeUpload, token, kind)
	req, err := http.NewRequest("POST", url, bodyBuf)
	req.Header.Add("Content-Type", bodyWriter.FormDataContentType())
	urlQuery := req.URL.Query()
	if err != nil {
		return "", err
	}

	req.URL.RawQuery = urlQuery.Encode()
	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	jsonbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	var result qiyeUploadResponse
	if err := json.Unmarshal(jsonbody, &result); err != nil {
		return "", err
	}

	if result.ErrCode != 0 {
		return "", fmt.Errorf("%d:%s", result.ErrCode, result.ErrMsg)
	}
	return result.MediaId, nil
}

func (c *WxQiye) SendCard(receiver []string, title, description, direct string, btn []*qiyeCardBtn) error {
	token, err := c.getToken()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s?access_token=%s", urlQiyeSend, token)

	var req qiyeMessageRequest
	req.ToUser = strings.Join(receiver, "|")
	req.AgentId = c.sender
	req.MstType = "taskcard"
	req.TaskCard.TaskId = fmt.Sprintf("%d_%d", time.Now().Unix(), time.Now().Nanosecond())
	req.TaskCard.Description = description
	req.TaskCard.Url = direct
	req.TaskCard.Title = title
	req.TaskCard.Btn = btn

	data, _ := json.Marshal(req)
	result, err := util.HttpPost(url).Body(data).ToString()
	if err != nil {
		return err
	}

	var rsp qiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}
	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}
