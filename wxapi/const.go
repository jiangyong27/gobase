package wxapi

const (
	urlWeappSendTemplete        = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send"
	urlWeappUniformSendTemplete = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send"
	urlWeappSend                = "https://api.weixin.qq.com/cgi-bin/message/custom/send"
	urlWeappUserOpenid          = "https://api.weixin.qq.com/cgi-bin/user/get"
	urlWeappUserInfo            = "https://api.weixin.qq.com/cgi-bin/user/info"
	urlWeappAllUserTag          = "https://api.weixin.qq.com/cgi-bin/tags/get"
	urlWeappUserByTag           = "https://api.weixin.qq.com/cgi-bin/user/tag/get"
	urlWeappBatchGetUserInfo    = "https://api.weixin.qq.com/cgi-bin/user/info/batchget"
)

const (
	bodyTypeXml  = "application/xml; charset=utf-8"
	bodyTypeJson = "application/json; charset=utf-8"
)

const (
	MsgTypeText  = "text"
	MsgTypeImage = "image"
	MsgTypeLink  = "link"
	MsgTypePage  = "miniprogrampage"
)
