package wxapi

import (
	"encoding/json"
	"fmt"

	"github.com/astaxie/beego/httplib"
	"github.com/silenceper/wechat"
	"github.com/silenceper/wechat/cache"
	"github.com/silenceper/wechat/miniprogram"
)

// 对外数据
type weappUserTag struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Count int    `json:"count"`
}

type weappUserInfo struct {
	Subscribe      int    `json:"subscribe"`
	Openid         string `json:"openid"`
	Nickname       string `json:"nickname"`
	Sex            int    `json:"sex"`
	Language       string `json:"language"`
	City           string `json:"city"`
	Province       string `json:"province"`
	Country        string `json:"country"`
	Headimgurl     string `json:"headimgurl"`
	SubscribeTime  int    `json:"subscribe_time"`
	Unionid        string `json:"unionid"`
	Remark         string `json:"remark"`
	Groupid        int    `json:"groupid"`
	TagidList      []int  `json:"tagid_list"`
	SubscribeScene string `json:"subscribe_scene"`
	QrScene        int    `json:"qr_scene"`
	QrSceneStr     string `json:"qr_scene_str"`
}

//对内数据
type weappTemplateData struct {
	Value string `json:"value"`
}

type weappTemplateMsg struct {
	TemplateId      string                        `json:"template_id"`
	FormId          string                        `json:"form_id"`
	Page            string                        `json:"page,omitempty"`
	EmphasisKeyword string                        `json:"emphasis_keyword,omitempty"`
	Data            map[string]*weappTemplateData `json:"data"`
}

type weappMpTemplateMsg struct {
	TemplateId  string `json:"template_id"`
	Appid       string `json:"appid"`
	Url         string `json:"url,omitempty"`
	Miniprogram struct {
		Appid    string `json:"appid"`
		Pagepath string `json:"pagepath"`
	} `json:"miniprogram"`
	Data map[string]*weappTemplateData `json:"data"`
}

type weappUniformTemplate struct {
	Touser   string              `json:"touser"`
	WeappMsg *weappTemplateMsg   `json:"weapp_template_msg,omitempty"`
	MpMsg    *weappMpTemplateMsg `json:"mp_template_msg,omitempty"`
}

type weappJsSession struct {
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionId    string `json:"union_id"`
}

type weappTempleteRequest struct {
	Touser           string `json:"touser"`
	WeappTemplateMsg struct {
		TemplateId string   `json:"template_id"`
		FormId     string   `json:"form_id"`
		Page       string   `json:"page,omitempty"`
		Data       []string `json:"data"`
	} `json:"weapp_msg"`

	MpTemplateMsg struct {
		Appid       string `json:"appid"`
		TemplateId  string `json:"template_id"`
		Url         string `json:"url,omitempty"`
		Miniprogram struct {
			Appid    string `json:"appid"`
			Pagepath string `json:"pagepath"`
		} `json:"miniprogram"`
		Data []string `json:"data"`
	} `json:"mp_msg"`
}

type weappCustomRequest struct {
	MsgType string `json:"msgtype"`
	Touser  string `json:"touser"`
	Text    struct {
		Content string `json:"content"`
	} `json:"text"`

	Image struct {
		MediaId string `json:"media_id"`
	} `json:"image"`
	Link struct {
		Title       string `json:"title"`
		Url         string `json:"url"`
		Description string `json:"description"`
		ThumbUrl    string `json:"thumb_url"`
	} `json:"linke"`

	Page struct {
		Title        string `json:"title"`
		PagePath     string `json:"pagepath"`
		ThumbMediaId string `json:"thumb_media_id"`
	} `json:"miniprogrampage"`
}

type weappBaseResonse struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

type WeappConfig struct {
	AppID     string
	AppSecret string
	Token     string
	AesKey    string
}

type Weapp struct {
	client *miniprogram.MiniProgram
	wc     *wechat.Wechat
}

func NewWeapp(cfg *WeappConfig) *Weapp {
	c := new(wechat.Config)
	c.AppID = cfg.AppID
	c.AppSecret = cfg.AppSecret
	c.Token = cfg.Token
	c.EncodingAESKey = cfg.AesKey
	c.Cache = cache.NewMemory()
	wc := wechat.NewWechat(c)
	mp := wc.GetMiniProgram()
	return &Weapp{
		client: mp,
		wc:     wc,
	}
}

func (c *Weapp) GetWechat() *wechat.Wechat {
	return c.wc
}

func (c *Weapp) GetAppid() string {
	return c.client.AppID
}

func (c *Weapp) Code2Session(code string) (*weappJsSession, error) {
	var rsp weappJsSession
	res, err := c.client.Code2Session(code)
	if err != nil {
		return nil, err
	}
	if res.ErrCode != 0 {
		return nil, fmt.Errorf("%d:%s", res.ErrCode, res.ErrMsg)
	}

	rsp.OpenId = res.OpenID
	rsp.SessionKey = res.SessionKey
	rsp.UnionId = res.UnionID
	return &rsp, nil
}

func (c *Weapp) GetUserPhone(sessionKey, encryptedData, iv string) (string, error) {
	userInfo, err := c.client.Decrypt(sessionKey, encryptedData, iv)
	if err != nil {
		return "", err
	}

	// todo return userInfo.PurePhoneNumber, nil
	return userInfo.NickName, nil
}

func (c *Weapp) GetOrderQrCode(page, sence string) ([]byte, error) {
	var param miniprogram.QRCoder
	param.Scene = sence
	param.Page = page
	return c.client.GetWXACodeUnlimit(param)
}

func (c *Weapp) SendTemplete(req *weappTempleteRequest) error {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return err
	}

	message := new(weappUniformTemplate)
	message.Touser = req.Touser
	message.WeappMsg = new(weappTemplateMsg)
	message.WeappMsg.TemplateId = req.WeappTemplateMsg.TemplateId
	message.WeappMsg.FormId = req.WeappTemplateMsg.FormId
	message.WeappMsg.Page = req.WeappTemplateMsg.Page
	message.WeappMsg.Data = make(map[string]*weappTemplateData)
	for i, dat := range req.WeappTemplateMsg.Data {
		item := new(weappTemplateData)
		item.Value = dat
		message.WeappMsg.Data[fmt.Sprintf("keyword%d", i+1)] = item
	}

	// todo优化公众号模板消息
	message.MpMsg = new(weappMpTemplateMsg)
	message.MpMsg.TemplateId = req.MpTemplateMsg.TemplateId
	message.MpMsg.Url = req.MpTemplateMsg.Url
	message.MpMsg.Appid = req.MpTemplateMsg.Appid
	message.MpMsg.Miniprogram.Appid = req.MpTemplateMsg.Miniprogram.Appid
	message.MpMsg.Miniprogram.Pagepath = req.MpTemplateMsg.Miniprogram.Pagepath
	for i, dat := range req.MpTemplateMsg.Data {
		item := new(weappTemplateData)
		item.Value = dat
		message.MpMsg.Data[fmt.Sprintf("keyword%d", i+1)] = item
	}

	reqBody, _ := json.Marshal(message)
	fmt.Println(string(reqBody))
	hc := httplib.Post(fmt.Sprintf("%s?access_token=%s", urlWeappUniformSendTemplete, accessToken))
	res, err := hc.Body(reqBody).Bytes()
	if err != nil {
		return err
	}
	var rsp weappBaseResonse
	err = json.Unmarshal(res, &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *Weapp) SendCustomText(user, content string) error {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return err
	}
	var req weappCustomRequest
	req.Text.Content = content
	req.Touser = user
	req.MsgType = MsgTypeText

	reqBody, _ := json.Marshal(req)
	hc := httplib.Post(fmt.Sprintf("%s?access_token=%s", urlWeappSend, accessToken))
	res, err := hc.Body(reqBody).Bytes()
	if err != nil {
		return err
	}
	var rsp weappBaseResonse
	err = json.Unmarshal(res, &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *Weapp) SendCustom(req *weappCustomRequest) error {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return err
	}

	reqBody, _ := json.Marshal(req)
	hc := httplib.Post(fmt.Sprintf("%s?access_token=%s", urlWeappSend, accessToken))
	res, err := hc.Body(reqBody).Bytes()
	if err != nil {
		return err
	}
	var rsp weappBaseResonse
	err = json.Unmarshal(res, &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *Weapp) GetAllUserTag() ([]*weappUserTag, error) {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return nil, err
	}
	res, err := httplib.Get(fmt.Sprintf("%s?access_token=%s", urlWeappAllUserTag, accessToken)).Bytes()
	if err != nil {
		return nil, err
	}
	type rsp_t struct {
		Tags []*weappUserTag `json:"tags"`
	}

	var rsp rsp_t
	if err := json.Unmarshal(res, &rsp); err != nil {
		return nil, fmt.Errorf("%s : %s", err.Error(), string(res))
	}
	return rsp.Tags, nil
}

func (c *Weapp) getOpenidByTag(tagId int) ([]string, error) {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return nil, err
	}
	reqBody := fmt.Sprintf(`{"tagid" : %d}`, tagId)
	url := fmt.Sprintf("%s?access_token=%s", urlWeappUserByTag, accessToken)
	res, err := httplib.Post(url).Body(reqBody).Bytes()
	if err != nil {
		return nil, err
	}

	type rsp_t struct {
		Count int `json:"count"`
		Data  struct {
			Openid []string `json:"openid"`
		} `json:"data"`
		NextOpenid string `json:"next_openid"`
	}

	var rsp rsp_t
	if err := json.Unmarshal(res, &rsp); err != nil {
		return nil, fmt.Errorf("%s : %s", err.Error(), string(res))
	}
	return rsp.Data.Openid, nil
}

func (c *Weapp) GetUserByTag(tagId int) ([]*weappUserInfo, error) {
	openids, err := c.getOpenidByTag(tagId)
	if err != nil {
		return nil, err
	}
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return nil, err
	}
	type req_info_t struct {
		Openid string `json:"openid"`
		Lang   string `json:"lang"`
	}
	type req_t struct {
		UserList []*req_info_t `json:"user_list"`
	}

	var req req_t
	req.UserList = make([]*req_info_t, 0)
	for _, openid := range openids {
		r := new(req_info_t)
		r.Openid = openid
		r.Lang = "zh_CN"
		req.UserList = append(req.UserList, r)
	}
	reqBody, _ := json.Marshal(req)
	url := fmt.Sprintf("%s?access_token=%s", urlWeappBatchGetUserInfo, accessToken)
	res, err := httplib.Post(url).Body(reqBody).Bytes()
	if err != nil {
		return nil, err
	}

	type rsp_t struct {
		UserInfoList []*weappUserInfo `json:"user_info_list"`
	}
	var rsp rsp_t
	if err := json.Unmarshal(res, &rsp); err != nil {
		return nil, fmt.Errorf("%s : %s", err.Error(), string(res))
	}
	return rsp.UserInfoList, nil
}
