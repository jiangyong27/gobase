package file

import (
	"io/ioutil"
	"os"
)

func Exist(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return false
}

func ListPath(dirPath string) ([]string, error) {
	lis, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}

	fileList := make([]string, 0)
	for _, f := range lis {
		if f.IsDir() {
			continue
		}
		fileList = append(fileList, f.Name())
	}
	return fileList, nil
}
