package file

import (
	"bufio"
	"io"
	"os"
	"strings"
)

type LineText struct {
	Text   string
	LineNo int64
}

type LineFunc func(line *LineText)

type Reader struct {
	filename string
	lineFunc LineFunc
	exitChan chan struct{}
}

func NewReader(filename string, proc func(line *LineText)) *Reader {
	_, err := os.Stat(filename)
	if err != nil {
		panic(err)
	}

	return &Reader{
		filename: filename,
		lineFunc : proc,
		exitChan: make(chan struct{}),
	}
}

func (r *Reader) Run() {

	file, err := os.Open(r.filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	readBuf := bufio.NewReader(file)
	lineno := int64(0)
	for {
		line, err := readBuf.ReadString('\n')
		if err != nil || io.EOF == err {
			break
		}
		lineno++
		line = strings.Trim(line, "\r\n\t ")
		lineText := &LineText{Text: line, LineNo: lineno}
		if r.lineFunc != nil {
			r.lineFunc(lineText)
		}
	}
	close(r.exitChan)
}

func (r *Reader) Join() {
	select {
	case <-r.exitChan:
	}
}
