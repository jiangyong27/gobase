package crypto

import (
	"fmt"
	"testing"
	"time"
)

func TestNewGoogleAuth(t *testing.T) {
	googleAuth := NewGoogleAuth()
	secret := googleAuth.GetSecret()

	qrcode := googleAuth.GetQrcode("test01", secret)

	for {
		code, _ := googleAuth.GetCode(secret)
		fmt.Println(code, secret, qrcode)
		time.Sleep(time.Second)
	}

}
