package crypto

import (
	"fmt"
	"testing"

	"gitlab.com/jiangyong27/gobase/util"
)

func TestDes(t *testing.T) {

	randomStr := util.RandomString(10)

	cipher, err := DesEncrypt([]byte(randomStr), []byte("12347890"))
	if err != nil {
		panic(err)
	}
	fmt.Println("cipher len:", len(cipher))

	origin, err := DesDecrypt(cipher, []byte("12347890"))
	if err != nil {
		panic(err)
	}
	fmt.Println("origin:", string(origin))

	if string(origin) != randomStr {
		t.Errorf("des failed")
	}
}
