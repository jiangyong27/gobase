package crypto

import (
	"bytes"
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"net/http"
	"sort"
	"strings"
)

const (
	SignTypeMd5  = 1
	SignTypeSha1 = 2
)

//md5在线校验：http://www.cmd5.com/
func signRaw(param map[string]string, secret string) []byte {
	arr := make([]string, 0)
	for k, _ := range param {
		arr = append(arr, k)
	}
	sort.Strings(arr)
	buffer := bytes.NewBuffer(nil)
	for _, key := range arr {
		buffer.WriteString(fmt.Sprintf("%s=%s&", key, param[key]))
	}
	buffer.WriteString(secret)
	return buffer.Bytes()
}

func SignMD5(param map[string]string, secret string) string {
	md := md5.Sum(signRaw(param, secret))
	sign := strings.ToLower(hex.EncodeToString(md[:]))
	return sign
}

func SignSha1(param map[string]string, secret string) string {
	md := sha1.Sum(signRaw(param, secret))
	sign := strings.ToLower(hex.EncodeToString(md[:]))
	return sign
}

func SignHttp(req *http.Request, secret, signKey string, signType int) string {
	if req.Form == nil {
		req.ParseForm()
	}
	param := make(map[string]string)
	for key, values := range req.Form {
		if key == signKey {
			continue
		}
		if len(values) == 0 {
			continue
		}
		param[key] = values[0]
	}

	if signType == SignTypeSha1 {
		return SignSha1(param, secret)
	} else {
		return SignMD5(param, secret)
	}
}
