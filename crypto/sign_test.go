package crypto

import (
	"fmt"
	"net/http"
	"net/url"
	"testing"
)

func TestSignMD5(t *testing.T) {
	param := make(map[string]string)
	param["jiangyong"] = "wuqing"
	param["aa"] = "bb"
	sign := SignMD5(param, "jiangyong")
	if sign != "0cb96ec8c73ad8b35c7ffea932ae4e58" {
		t.Errorf("sign failed")
	}
	fmt.Println("sign:", sign)
}

func TestSignSha1(t *testing.T) {
	param := make(map[string]string)
	param["jiangyong"] = "wuqing"
	param["aa"] = "bb"
	sign := SignSha1(param, "jiangyong")
	if sign != "55cdf8509095015258222278a119913dad96c055" {
		t.Errorf("sign failed")
	}
	fmt.Println("sign:", sign)
}

func TestSignHttp(t *testing.T) {
	var req http.Request
	var values url.Values
	values = make(url.Values)

	values.Add("jiangyong", "wuqing")
	values.Add("aa", "bb")
	req.Form = values

	sign := SignHttp(&req, "jiangyong", "sign", SignTypeMd5)
	fmt.Println("sign:", sign)
	if sign != "0cb96ec8c73ad8b35c7ffea932ae4e58" {
		t.Errorf("sign failed")
	}

}
