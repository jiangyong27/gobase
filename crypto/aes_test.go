package crypto

import (
	"fmt"
	"testing"

	"gitlab.com/jiangyong27/gobase/util"
)

func TestAes(t *testing.T) {
	randomStr := util.RandomString(10)

	fmt.Println("origin:", randomStr)
	cipher, err := AesEncrypt([]byte(randomStr), []byte("1234789012347890"))
	if err != nil {
		panic(err)
	}
	fmt.Println("cipher len:", len(cipher))

	origin, err := AesDecrypt(cipher, []byte("1234789012347890"))
	if err != nil {
		panic(err)
	}
	fmt.Println("origin:", string(origin))

	if string(origin) != randomStr {
		t.Errorf("TestAes failed")
	}
}
