package apicurd

import (
	"fmt"
	"net/http"

	"github.com/Knetic/govaluate"

	"github.com/gin-gonic/gin"
)

type apiDelete struct {
	table     *table
	conf      *apiConf
	handler   gin.HandlerFunc
	checkFunc []*govaluate.EvaluableExpression
}

func (a *apiDelete) init() error {
	var err error

	a.checkFunc, err = createFuncExpr(a.conf.Delete.Check)
	if err != nil {
		return err
	}

	a.handler = createHandler(a.conf.Delete.Handler)

	return nil
}

func (a *apiDelete) serve(ctx *gin.Context) {
	if a.handler != nil {
		a.handler(ctx)
		return
	}

	if a.conf.Delete.Filter == nil || len(a.conf.Delete.Filter) == 0 {
		panic(errInternal.New())
	}
	filter := a.genFilter(ctx)
	if filter == nil || len(filter) == 0 {
		panic(errParam.New().Append("no filter"))
	}

	// 2.校验函数
	for i, fn := range a.checkFunc {
		_, err := fn.Evaluate(filter)
		if err != nil {
			panic(errInternal.New().Append(
				fmt.Errorf("[%s] %s", a.conf.Delete.Check[i], err)))
		}
	}

	delcnt, err := a.table.delete(filter)
	if err != nil {
		panic(errInternal.New().Append(err))
	}
	ctx.JSON(http.StatusOK, newRsp(delcnt))

}

func (a *apiDelete) genFilter(ctx *gin.Context) map[string]interface{} {
	res := make(map[string]interface{})
	for k, _ := range a.conf.Delete.Filter {
		val, ok := ctx.GetQuery(k)
		if !ok {
			continue
		}
		res[k] = val
	}
	return res
}
