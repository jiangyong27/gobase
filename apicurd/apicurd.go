package apicurd

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"strings"

	"github.com/gin-gonic/gin"
)

type ApiCurd struct {
	config   *config
	defDB    *database
	bizDB    map[string]*database
	apis     map[string]*apiItem
	handlers []gin.HandlerFunc
}

func New(handlers ...handler) *ApiCurd {
	config := new(config)
	for _, handler := range handlers {
		handler(config)
	}
	return &ApiCurd{
		config:   config,
		handlers: make([]gin.HandlerFunc, 0),
	}
}

func (a *ApiCurd) Use(handler gin.HandlerFunc) {
	a.handlers = append(a.handlers, handler)
}

func (a *ApiCurd) Init() error {
	if err := a.initDB(); err != nil {
		return err
	}
	if err := a.initScan(); err != nil {
		return err
	}

	return nil
}

func (a *ApiCurd) initDB() error {
	defDB := &database{
		db: a.config.defDB,
	}
	if err := defDB.load(); err != nil {
		return err
	}
	a.defDB = defDB

	a.bizDB = make(map[string]*database)
	for k, v := range a.config.bizDB {
		tmpDB := &database{
			db: v,
		}
		if err := tmpDB.load(); err != nil {
			return err
		}
		a.bizDB[k] = tmpDB
	}
	return nil
}

func (a *ApiCurd) initScan() error {
	if a.config.dir == "" {
		return errors.New("no SetScanDir")
	}

	fileInfos, err := ioutil.ReadDir(a.config.dir)
	if err != nil {
		return err
	}

	a.apis = make(map[string]*apiItem)
	for _, fileInfo := range fileInfos {
		if fileInfo.IsDir() {
			continue
		}
		if err := a.load(path.Join(a.config.dir, fileInfo.Name())); err != nil {
			return err
		}
	}
	return nil
}

func (a *ApiCurd) serve(ctx *gin.Context) {

	key := ctx.Request.Method + "_" + ctx.Request.URL.Path
	if api, ok := a.apis[key]; ok {
		api.serve(ctx)
		return
	}

	ctx.JSON(http.StatusNotFound, nil)
}

func (a *ApiCurd) GetRouteInfo() gin.RoutesInfo {
	res := make([]gin.RouteInfo, 0)
	for k, v := range a.apis {
		fields := strings.SplitN(k, "_", 2)
		res = append(res, gin.RouteInfo{
			Path:   v.conf.Path,
			Method: fields[0],
		})
	}
	return res
}

func (a *ApiCurd) load(file string) error {
	body, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	var apiConfs []*apiConf
	if err := json.Unmarshal(body, &apiConfs); err != nil {
		return err
	}

	group := a.config.engine.Group("/")
	group.Use(a.handlers...)

	for _, ac := range apiConfs {
		if err := ac.check(); err != nil {
			return err
		}
		item := &apiItem{conf: ac}
		if err := item.init(a); err != nil {
			return err
		}

		for _, m := range ac.Method {
			key := fmt.Sprintf("%s_%s", m, ac.Path)
			if _, ok := a.apis[ac.getKey()]; ok {
				return fmt.Errorf("%s repeat", ac.getKey())
			}
			group.Handle(m, ac.Path, a.serve)
			a.apis[key] = item
		}
	}
	return nil
}
