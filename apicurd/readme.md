# ApiCurd
![](https://img.shields.io/github/stars/ehang-io/nps.svg)   ![](https://img.shields.io/github/forks/ehang-io/nps.svg)
[![Gitter](https://badges.gitter.im/cnlh-nps/community.svg)](https://gitter.im/cnlh-nps/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
![Release](https://github.com/ehang-io/nps/workflows/Release/badge.svg)
![GitHub All Releases](https://img.shields.io/github/downloads/ehang-io/nps/total)

一个通过配置化的restful接口的插件，通过json配置文件制定标准的restful接口。只需一个简单的配置文件就可实现基础数据的增、删、查、改接口，支持表达式、自定义函数、自定义handler，可扩展性强。轻易的配置出通用的api服务器，非常使用于没有后端基础的前端开发人员。
##新增

##删除
check
filter

##查询

###更新
* filter规则对url参数和body里的参数，url参数优先级高（body参数经过表达式之后作为过滤条件）
* check规则对url参数和body参数都生效，body参数优先级高