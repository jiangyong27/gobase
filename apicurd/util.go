package apicurd

func mapMerge(args ...map[string]interface{}) map[string]interface{} {
	res := make(map[string]interface{})
	for _, m := range args {
		for k, v := range m {
			res[k] = v
		}
	}

	return res
}
