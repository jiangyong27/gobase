package apicurd

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/Knetic/govaluate"
	"github.com/gin-gonic/gin"
	"gitlab.com/jiangyong27/gobase/util"
)

type apiQuery struct {
	table      *table
	conf       *apiConf
	filterFunc map[string]*govaluate.EvaluableExpression
	fieldFunc  map[string]*govaluate.EvaluableExpression
	checkFunc  []*govaluate.EvaluableExpression
	handler    gin.HandlerFunc
}

func (a *apiQuery) init() error {
	var err error
	a.fieldFunc, err = createFieldExpr(a.conf.Query.Field)
	if err != nil {
		return err
	}

	a.checkFunc, err = createFuncExpr(a.conf.Query.Check)
	if err != nil {
		return err
	}
	a.handler = createHandler(a.conf.Query.Handler)
	return nil
}

func (a *apiQuery) serve(ctx *gin.Context) {
	var err error
	if a.handler != nil {
		a.handler(ctx)
		return
	}

	// 1.前置函数
	filter := a.genFilter(ctx)
	for i, fn := range a.checkFunc {
		_, err = fn.Evaluate(filter)
		if err != nil {
			panic(errInternal.New().Append(
				fmt.Errorf("[%s] %s", a.conf.Query.Check[i], err)))
		}
	}

	// 2.校验函数
	for i, fn := range a.checkFunc {
		_, err := fn.Evaluate(filter)
		if err != nil {
			panic(errInternal.New().Append(
				fmt.Errorf("[%s] %s", a.conf.Delete.Check[i], err)))
		}
	}

	// 2.查询数据库
	page, size := a.genPageSize(ctx)
	sort := a.genSort(ctx)
	rows, total, err := a.table.query(nil, filter, sort, page, size)
	if err != nil {
		panic(errInternal.New().Append(err))
	}

	list := make([]map[string]interface{}, 0)
	for _, row := range rows {
		r := make(map[string]interface{})
		for k, _ := range a.conf.Query.Field {
			expr := a.fieldFunc[k]
			res, err := expr.Evaluate(row)
			if err != nil {
				panic(errInternal.New().Append(err))
			}
			r[k] = res
		}
		list = append(list, r)
	}
	result := make(map[string]interface{})
	result["total"] = total
	result["list"] = list
	ctx.JSON(http.StatusOK, newRsp(result))
}

func (a *apiQuery) genSort(ctx *gin.Context) string {
	sort := a.conf.Query.Sort
	fields := strings.SplitN(sort, ",", 2)
	if len(fields) != 2 {
		return ""
	}
	if s, ok := ctx.GetQuery(fields[0]); ok {
		return s
	}
	return fields[1]
}

func (a *apiQuery) genPageSize(ctx *gin.Context) (int, int) {
	page := 1
	size := 10

	fields := strings.SplitN(a.conf.Query.Page, ",", 2)
	if len(fields) == 2 {
		if s, ok := ctx.GetQuery(fields[0]); ok {
			page = util.GetInt(s)
		} else {
			page = util.GetInt(fields[1])
		}
	}

	fields = strings.SplitN(a.conf.Query.Size, ",", 2)
	if len(fields) == 2 {
		if s, ok := ctx.GetQuery(fields[0]); ok {
			size = util.GetInt(s)
		} else {
			size = util.GetInt(fields[1])
		}
	}

	return page, size
}

func (a *apiQuery) genFilter(ctx *gin.Context) map[string]interface{} {

	res := make(map[string]interface{})
	for k, v := range a.conf.Query.Filter {
		val, ok := ctx.GetQuery(v)
		if !ok {
			continue
		}
		res[k] = val
	}
	return res
}
