package apicurd

import "github.com/gogap/errors"

var (
	errInternal = errors.T(300, "内部错误")
	errParam    = errors.T(301, "参数错误")
)

type apiRsp struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

func newRsp(data interface{}) *apiRsp {
	r := new(apiRsp)
	r.Code = 0
	r.Message = "操作成功"
	r.Data = data
	return r
}
