package apicurd

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type apiMethod interface {
	serve(ctx *gin.Context)
	init() error
}

type apiItem struct {
	conf       *apiConf
	table      *table
	apiMethods map[string]apiMethod
}

func (a *apiConf) getKey() string {
	return fmt.Sprintf("%s_%s", a.Method, a.Path)
}

func (a *apiItem) init(curd *ApiCurd) error {
	table, err := a.initTable(curd)
	if err != nil {
		return err
	}
	a.apiMethods = make(map[string]apiMethod)
	a.apiMethods[http.MethodPost] = &apiCreate{table: table, conf: a.conf}
	a.apiMethods[http.MethodDelete] = &apiDelete{table: table, conf: a.conf}
	a.apiMethods[http.MethodGet] = &apiQuery{table: table, conf: a.conf}
	a.apiMethods[http.MethodPut] = &apiUpdate{table: table, conf: a.conf}

	for _, serv := range a.apiMethods {
		if err := serv.init(); err != nil {
			return err
		}
	}

	return nil
}

func (a *apiItem) initTable(curd *ApiCurd) (*table, error) {
	if a.conf.Table == "" {
		return nil, nil
	}
	fields := strings.Split(a.conf.Table, ".")
	var table *table = nil
	if len(fields) == 2 {
		if _, ok := curd.bizDB[fields[0]]; !ok {
			return nil, fmt.Errorf("db[%s] not config", fields[0])
		}
		table = curd.bizDB[fields[0]].getTable(fields[1])
	} else {
		table = curd.defDB.getTable(a.conf.Table)
	}
	if table == nil {
		return nil, errors.New("table not exist")
	}
	return table, nil
}

func (a *apiItem) serve(ctx *gin.Context) {

	method := strings.ToUpper(ctx.Request.Method)
	if serv, ok := a.apiMethods[method]; ok {
		serv.serve(ctx)
		return
	}
	panic(errInternal.New().Append(errors.New("method not support")))
}
