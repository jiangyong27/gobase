package apicurd

type apiConf struct {
	Path   string   `json:"path"`
	Method []string `json:"method"`
	Table  string   `json:"table"`
	Create struct {
		Field   map[string]string `json:"field"`
		Check   []string          `json:"check"`
		Handler string            `json:"handler"`
	} `json:"create"`

	Delete struct {
		Filter  map[string]string `json:"filter"`
		Check   []string          `json:"check"`
		Handler string            `json:"handler"`
	} `json:"delete"`

	Update struct {
		Filter  map[string]string `json:"filter"`
		Field   map[string]string `json:"field"`
		Check   []string          `json:"check"`
		Handler string            `json:"handler"`
	}

	Query struct {
		Page    string            `json:"page"`
		Size    string            `json:"size"`
		Sort    string            `json:"sort"`
		Filter  map[string]string `json:"filter"`
		Field   map[string]string `json:"field"`
		Check   []string          `json:"check"`
		Handler string            `json:"handler"`
	} `json:"query"`
}

func (a *apiConf) check() error {
	return nil
}
