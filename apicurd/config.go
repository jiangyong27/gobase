package apicurd

import (
	"errors"
	"os"

	"github.com/gin-gonic/gin"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type config struct {
	defDB  *gorm.DB
	bizDB  map[string]*gorm.DB
	dir    string
	engine *gin.Engine
}

type handler func(config *config)

func SetScanDir(dir string) handler {
	fileInfo, err := os.Stat(dir)
	if err != nil {
		panic(err)
	}
	if !fileInfo.IsDir() {
		panic(errors.New("no dir for scan difr"))
	}
	return func(config *config) {
		config.dir = dir
	}
}

func SetGinEngine(engine *gin.Engine) handler {
	if engine == nil {
		panic(errors.New("engine is nil"))
	}

	return func(config *config) {
		config.engine = engine
	}
}

func SetDefaultDb(db *gorm.DB) handler {
	if db == nil {
		panic(errors.New("db is nil"))
	}

	return func(config *config) {
		config.defDB = db
	}
}

func SetDefaultDsn(dsn string) handler {
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	return func(config *config) {
		config.defDB = db
	}
}

func SetDsn(name, dsn string) handler {
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	return func(config *config) {
		if config.bizDB == nil {
			config.bizDB = make(map[string]*gorm.DB)
		}
		config.bizDB[name] = db
	}
}
