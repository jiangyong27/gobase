package apicurd

import (
	"gorm.io/gorm"
)

type database struct {
	tables map[string]*table
	db     *gorm.DB
}

func (d *database) getTable(name string) *table {
	if tb, ok := d.tables[name]; ok {
		return tb
	}
	return nil
}

func (d *database) load() error {
	sqlDb, err := d.db.DB()
	if err != nil {
		return err
	}
	sql := "SHOW TABLES"

	rows, err := sqlDb.Query(sql)
	if err != nil {
		return err
	}
	tables := make(map[string]*table, 0)
	for rows.Next() {
		var tableName string
		if err := rows.Scan(&tableName); err != nil {
			return err
		}
		table := &table{
			name: tableName,
			db:   d.db,
		}
		if err := table.load(); err != nil {
			return err
		}
		tables[tableName] = table
	}
	d.tables = tables
	return nil
}
