package apicurd

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/Knetic/govaluate"
	"github.com/gin-gonic/gin"
)

type apiCreate struct {
	table     *table
	conf      *apiConf
	fieldFunc map[string]*govaluate.EvaluableExpression
	checkFunc []*govaluate.EvaluableExpression
	handler   gin.HandlerFunc
}

func (a *apiCreate) init() error {
	var err error
	a.fieldFunc, err = createFieldExpr(a.conf.Create.Field)
	if err != nil {
		return err
	}

	a.checkFunc, err = createFuncExpr(a.conf.Create.Check)
	if err != nil {
		return err
	}
	a.handler = createHandler(a.conf.Create.Handler)
	return nil
}

func (a *apiCreate) serve(ctx *gin.Context) {
	if a.handler != nil {
		a.handler(ctx)
		return
	}

	body, err := ctx.GetRawData()
	if err != nil {
		panic(errInternal.New().Append(err))
	}
	params := make(map[string]interface{})
	if err := json.Unmarshal(body, &params); err != nil {
		panic(errParam.New().Append(err))
	}

	// 1.数据字段计算
	data := make(map[string]interface{})
	for k, expr := range a.fieldFunc {
		res, err := expr.Evaluate(params)
		if err != nil {
			continue
		}
		data[k] = res
	}

	// 2.校验函数
	for i, fn := range a.checkFunc {
		_, err = fn.Evaluate(data)
		if err != nil {
			panic(errInternal.New().Append(
				fmt.Errorf("[%s] %s", a.conf.Create.Check[i], err)))
		}
	}

	// 3.插入函数
	if err := a.table.create(data); err != nil {
		panic(errParam.New().Append(err))
	}

	ctx.JSON(http.StatusOK, newRsp(nil))
}
