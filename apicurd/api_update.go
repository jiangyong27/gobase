package apicurd

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/Knetic/govaluate"
	"github.com/gin-gonic/gin"
)

type apiUpdate struct {
	table     *table
	conf      *apiConf
	fieldFunc map[string]*govaluate.EvaluableExpression
	checkFunc []*govaluate.EvaluableExpression
	handler   gin.HandlerFunc
}

func (a *apiUpdate) init() error {
	var err error
	a.fieldFunc, err = createFieldExpr(a.conf.Update.Field)
	if err != nil {
		return err
	}

	a.checkFunc, err = createFuncExpr(a.conf.Create.Check)
	if err != nil {
		return err
	}
	a.handler = createHandler(a.conf.Update.Handler)
	return nil
}

func (a *apiUpdate) serve(ctx *gin.Context) {
	if a.handler != nil {
		a.handler(ctx)
		return
	}

	body, err := ctx.GetRawData()
	if err != nil {
		panic(errInternal.New().Append(err))
	}

	params := make(map[string]interface{})
	if err := json.Unmarshal(body, &params); err != nil {
		panic(errParam.New().Append(err))
	}

	// 1.数据准备
	data := make(map[string]interface{})
	for k, expr := range a.fieldFunc {
		res, err := expr.Evaluate(params)
		if err != nil {
			continue
		}
		data[k] = res
	}

	// 2.前置函数
	filter := a.genFilter(ctx, mapMerge(params, data))
	checkData := mapMerge(filter, data)
	for i, fn := range a.checkFunc {
		_, err = fn.Evaluate(checkData)
		if err != nil {
			panic(errInternal.New().Append(
				fmt.Errorf("[%s] %s", a.conf.Create.Check[i], err)))
		}
	}

	// 3.更新函数
	if err := a.table.update(data, filter); err != nil {
		panic(errInternal.New().Append(err))
	}

	ctx.JSON(http.StatusOK, newRsp(nil))
	return
}

func (a *apiUpdate) genFilter(ctx *gin.Context, data map[string]interface{}) map[string]interface{} {
	res := make(map[string]interface{})
	for k, v := range a.conf.Update.Filter {
		val, ok := ctx.GetQuery(v)
		if !ok {
			if d, ok := data[v]; ok {
				res[k] = d
			}
			continue
		}
		res[k] = val
	}
	return res
}
