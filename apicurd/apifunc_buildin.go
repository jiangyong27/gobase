package apicurd

import (
	"encoding/json"
	"errors"
	"time"

	"gitlab.com/jiangyong27/gobase/util"
)

func unixTime(args ...interface{}) (interface{}, error) {
	return time.Now().Unix(), nil
}

func unixTimeFormat(args ...interface{}) (interface{}, error) {
	if len(args) == 0 {
		return nil, errors.New("no unixtime input")
	}

	t := time.Unix(util.GetInt64(args[0]), 0)
	if len(args) >= 2 {
		return t.Format(util.GetString(args[1])), nil
	}
	return t.Format("2006-01-02 15:04:05"), nil
}

func unpackJson(args ...interface{}) (interface{}, error) {
	if len(args) != 2 {
		return nil, errors.New("extractFromJson parameter error")
	}

	if args[0] == nil {
		return nil, nil
	}

	var err error
	mp := make(map[string]interface{})
	switch result := args[0].(type) {
	case string:
		err = json.Unmarshal([]byte(result), &mp)
	case []byte:
		err = json.Unmarshal(result, &mp)
	default:
		err = errors.New("no support type")
	}
	if err != nil {
		return nil, err
	}

	return mp[util.GetString(args[1])], nil
}

func intToBool(args ...interface{}) (interface{}, error) {
	if len(args) == 0 {
		return false, nil
	}
	if util.GetInt(args[0]) == 0 {
		return false, nil
	}
	return true, nil
}

func boolToInt(args ...interface{}) (interface{}, error) {
	if len(args) == 0 {
		return 0, nil
	}
	return util.GetBool(args[0]), nil
}

func null(args ...interface{}) (interface{}, error) {
	return nil, nil
}
