package apicurd

import (
	"gorm.io/gorm"
)

type table struct {
	db     *gorm.DB
	name   string
	fTypes map[string]string
	fields []string
}

func (t *table) load() error {
	sqlDb, err := t.db.DB()
	if err != nil {
		return err
	}
	sql := "DESC " + t.name

	rows, err := sqlDb.Query(sql)
	if err != nil {
		return err
	}
	fields := make([]string, 0)
	fTypes := make(map[string]string)
	for rows.Next() {
		var field string
		var tp string
		var skip interface{}
		if err := rows.Scan(&field, &tp,
			&skip, &skip, &skip, &skip); err != nil {
			return err
		}
		fields = append(fields, field)
		fTypes[field] = tp
	}
	t.fields = fields
	t.fTypes = fTypes
	return nil
}

func (t *table) update(data map[string]interface{}, filter map[string]interface{}) error {
	tx := t.db.Table(t.name)

	if filter != nil {
		tx = tx.Where(filter)
	}
	tx = tx.Updates(data)
	return tx.Error
}

func (t *table) delete(filter map[string]interface{}) (int64, error) {
	tx := t.db.Table(t.name)

	if filter != nil {
		tx = tx.Where(filter)
	}
	tx = tx.Delete(nil)
	return tx.RowsAffected, tx.Error
}

func (t *table) create(data map[string]interface{}) error {
	tx := t.db.Table(t.name)
	tx = tx.Model(nil).Create(&data)
	if tx.Error != nil {
		return tx.Error
	}

	return nil
}

func (t *table) query(fields []string, filter map[string]interface{},
	order string, page int, size int) ([]map[string]interface{}, int64, error) {

	if fields == nil {
		fields = t.fields
	}

	tx := t.db.Table(t.name)

	if filter != nil {
		tx = tx.Where(filter)
	}
	count := int64(0)
	tx = tx.Count(&count)
	if order != "" {
		tx = tx.Order(order)
	}
	if page != 0 && size != 0 {
		tx = tx.Offset((page - 1) * size).Limit(size)
	}

	tx = tx.Model(nil)
	datas := make([]map[string]interface{}, 0)
	tx = tx.Select(fields).Find(&datas)
	if tx.Error != nil {
		return nil, 0, tx.Error
	}
	return datas, count, nil
}
