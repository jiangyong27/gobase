package apicurd

import (
	"errors"

	"github.com/gin-gonic/gin"

	"github.com/Knetic/govaluate"
)

var (
	allExprFuncs   map[string]govaluate.ExpressionFunction = nil
	allGinHandlers map[string]gin.HandlerFunc              = nil
)

func init() {
	allExprFuncs = make(map[string]govaluate.ExpressionFunction)
	allGinHandlers = make(map[string]gin.HandlerFunc)
	allExprFuncs["unixTime"] = unixTime
	allExprFuncs["unixTimeFormat"] = unixTimeFormat
	allExprFuncs["unpackJson"] = unpackJson
	allExprFuncs["intToBool"] = intToBool
	allExprFuncs["boolToInt"] = boolToInt
	allExprFuncs["null"] = null
}

func AddHandler(name string, handler gin.HandlerFunc) {
	if _, ok := allGinHandlers[name]; ok {
		panic(errors.New("handler already exist"))
	}
	allGinHandlers[name] = handler
}

func AddFunc(name string, fn govaluate.ExpressionFunction) {
	if _, ok := allExprFuncs[name]; ok {
		panic(errors.New("func already exist"))
	}
	allExprFuncs[name] = fn
}

func createHandler(handler string) gin.HandlerFunc {
	if h, ok := allGinHandlers[handler]; ok {
		return h
	}
	return nil
}

func createFieldExpr(field map[string]string) (map[string]*govaluate.EvaluableExpression, error) {
	funcs := make(map[string]*govaluate.EvaluableExpression)
	for k, v := range field {
		expr, err := govaluate.NewEvaluableExpressionWithFunctions(v, allExprFuncs)
		if err != nil {
			return nil, err
		}
		funcs[k] = expr
	}
	return funcs, nil
}

func createFuncExpr(fArr []string) ([]*govaluate.EvaluableExpression, error) {
	funcs := make([]*govaluate.EvaluableExpression, 0)
	for _, v := range fArr {
		expr, err := govaluate.NewEvaluableExpressionWithFunctions(v, allExprFuncs)
		if err != nil {
			return nil, err
		}
		funcs = append(funcs, expr)
	}
	return funcs, nil
}
