package store

import (
	"errors"
	"time"

	"sync"
)

//Memcache struct contains *memcache.Client
type dataNode struct {
	data       string
	expireTime time.Time
}
type Memcache struct {
	data     map[string]*dataNode
	interval int
	lock     sync.Mutex
}

//NewMemcache create new memcache
func NewMemcache() *Memcache {

	return &Memcache{
		data:     make(map[string]*dataNode),
		interval: 60,
	}
}

//Get return cached value
func (mem *Memcache) Get(key string) interface{} {
	mem.lock.Lock()
	defer mem.lock.Unlock()

	if node, ok := mem.data[key]; ok {
		if time.Now().After(node.expireTime) {
			delete(mem.data, key)
			return nil
		}
		return node.data
	}

	return nil
}

// IsExist check value exists in memcache.
func (mem *Memcache) IsExist(key string) bool {
	mem.lock.Lock()
	defer mem.lock.Unlock()

	if node, ok := mem.data[key]; ok {
		if time.Now().After(node.expireTime) {
			delete(mem.data, key)
			return false
		}
		return true
	}
	return false
}

//Set cached value with key and expire time.
func (mem *Memcache) Set(key string, val interface{}, timeout time.Duration) error {
	v, ok := val.(string)
	if !ok {
		return errors.New("val must string")
	}
	mem.lock.Lock()
	defer mem.lock.Unlock()

	node := new(dataNode)
	node.expireTime = time.Now().Add(timeout)
	node.data = v
	mem.data[key] = node
	return nil
}

//Delete delete value in memcache.
func (mem *Memcache) Del(key string) error {
	mem.lock.Lock()
	defer mem.lock.Unlock()
	delete(mem.data, key)
	return nil

}
