package store

import "testing"

func TestBitMap(t *testing.T) {
	bm :=NewBitMap(100)
	bm.Set(10)
	if bm.Get(10) != true {
		t.Errorf("bitmap get error")
	}

	if bm.Get(11) != false {
		t.Errorf("bitmap get error")
	}
}
