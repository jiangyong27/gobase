package store

import (
	"testing"
	"time"
)

func TestMemcache(t *testing.T) {
	mem := NewMemcache()
	mem.Set("jiangyong", "wuqing", time.Second * 1)

	val := mem.Get("jiangyong")
	if val.(string) != "wuqing" {
		t.Errorf("memcache key/value error1")
	}

	time.Sleep(2 * time.Second)
	if mem.IsExist("jiangyong") {
		t.Errorf("memcache key/value error2")
	}
}