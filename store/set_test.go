package store

import (
	"fmt"
	"testing"
)

func TestSet(t *testing.T) {
	ss :=NewSet()
	ss.Add(1)
	ss.Add(2)
	ss.Add(1)
	if ss.Len() != 2 {
		t.Errorf("set unique error")
	}

	ss.Remove(1)
	if ss.Len() != 1 {
		t.Errorf("set unique error")
	}
	fmt.Println(ss.List())
}
